const buildApp = require('../api/app');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const ItemsModel = require('../database/itemsModel');
const uuid = require('uuid');

describe('Deletes an item: /api/items/:id', () => {
  let server;
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );

    server = buildApp();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
    server.close();
  });

  test('Successfully deletes an item', async () => {
    // given
    const savedItemId = uuid.v4();
    const savedItem = new ItemsModel({
      name: 'some-name',
      timestamp: new Date().toISOString(),
      _id: savedItemId
    });
    await savedItem.save();

    // when
    const response = await server.inject({
      method: 'DELETE',
      url: `/api/items/${savedItemId}`
    });
    const findItem = await ItemsModel.findById(savedItemId).lean();

    // then
    expect(response.statusCode).toBe(204);
    expect(response.body).toBe('');
    expect(findItem).toBeNull();

    await mongoose.connection.dropDatabase();
  });

  test('Returns "404 not found" if the id can not be found in the database', async () => {
    // given
    const id = uuid.v4();

    // when
    const response = await server.inject({
      method: 'DELETE',
      url: `/api/items/${id}`
    });

    // then
    expect(response.statusCode).toBe(404);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '404',
        error: 'Not Found',
        message: `The item with the id ${id} does not exist in the database.`
      })
    );
  });

  test('Failing parameter id validation sends an error ', async () => {
    // given
    const id = 'some-non-uuid';

    // when
    const response = await server.inject({
      method: 'DELETE',
      url: `/api/items/${id}`
    });

    // then
    expect(response.statusCode).toBe(400);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '400',
        error: 'Bad Request',
        message: 'Request parameter id is not a UUID.'
      })
    );
  });

  test('Failing database sends an error', async () => {
    // given
    const id = uuid.v4();
    await mongoose.disconnect();

    // when
    const response = await server.inject({
      method: 'DELETE',
      url: `/api/items/${id}`
    });

    // then
    expect(response.statusCode).toBe(500);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '500',
        error: 'Internal Server Error',
        message: 'MongoClient must be connected to perform this operation'
      })
    );
  });
});
