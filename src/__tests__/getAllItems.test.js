const buildApp = require('../api/app');
const mongoose = require('mongoose');
const ItemsModel = require('../database/itemsModel');
const uuid = require('uuid');
const { MongoMemoryServer } = require('mongodb-memory-server');

describe('Gets all items: /api/items', () => {
  let server;
  let mongoServer;

  beforeAll(async () => {
    mongoServer = await MongoMemoryServer.create();
    await mongoose.connect(
      mongoServer.getUri(),
      {}
    );

    server = buildApp();
  });

  afterAll(async () => {
    await mongoose.disconnect();
    await mongoServer.stop();
    server.close();
  });

  test('Successfully gets all items', async () => {
    // given
    const itemId1 = uuid.v4();
    const item1 = {
      name: 'some-name',
      timestamp: new Date().toISOString(),
      category: 'some-category'
    };
    await new ItemsModel({
      ...item1,
      _id: itemId1
    }).save();

    const itemId2 = uuid.v4();
    const item2 = {
      name: 'some-other-name',
      timestamp: new Date().toISOString()
    };
    await new ItemsModel({
      ...item2,
      _id: itemId2
    }).save();

    // when
    const response = await server.inject({
      method: 'GET',
      url: '/api/items'
    });

    // then
    expect(response.statusCode).toBe(200);
    expect(response.headers['content-type']).toBe(
      'application/hal+json; charset=utf-8'
    );
    expect(response.body).toBe(
      JSON.stringify([
        {
          ...item1,
          _links: { self: { href: `/api/items/${itemId1}` } }
        },
        {
          ...item2,
          _links: { self: { href: `/api/items/${itemId2}` } }
        }
      ])
    );

    await mongoose.connection.dropDatabase();
  });

  test('Returns "404 not found" there are no items found in the database', async () => {
    // when
    const response = await server.inject({ method: 'GET', url: '/api/items' });

    //then
    expect(response.statusCode).toBe(404);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '404',
        error: 'Not Found',
        message: 'There are no items stored in the database.'
      })
    );
  });

  test('Failing database sends an error', async () => {
    // given
    await mongoose.disconnect();

    // when
    const response = await server.inject({ method: 'GET', url: '/api/items' });

    // then
    expect(response.statusCode).toBe(500);
    expect(response.headers['content-type']).toBe(
      'application/json; charset=utf-8'
    );
    expect(response.body).toEqual(
      JSON.stringify({
        statusCode: '500',
        error: 'Internal Server Error',
        message: 'MongoClient must be connected to perform this operation'
      })
    );
  });
});
