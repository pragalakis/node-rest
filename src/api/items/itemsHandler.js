const mongoose = require('mongoose');
const ItemsModel = require('../../database/itemsModel');
const uuid = require('uuid');

// Gets all items
exports.getItems = async (request, response) => {
  try {
    const items = await ItemsModel.find().lean();
    if (items.length === 0) {
      response.status(404);
      throw new Error('There are no items stored in the database.');
    } else {
      const itemsWithLinks = items.map((item) => ({
        name: item.name,
        timestamp: item.timestamp,
        category: item.category,
        _links: { self: { href: `/api/items/${item._id}` } }
      }));

      response
        .code(200)
        .header('Content-Type', 'application/hal+json; charset=utf-8')
        .send(itemsWithLinks);
    }
  } catch (error) {
    throw error;
  }
};

// Gets a single item by ID
exports.getSingleItem = async (request, response) => {
  try {
    const id = request.params.id;
    const item = await ItemsModel.findById(id).lean();

    if (item === null) {
      response.status(404);
      throw new Error(
        `The item with the id ${id} does not exist in the database.`
      );
    } else {
      request.log.info(`ID: ${item._id} retrieved from the database.`);

      const itemWithLink = {
        name: item.name,
        timestamp: item.timestamp,
        category: item.category,
        _links: { self: { href: request.url } }
      };

      response
        .code(200)
        .header('Content-Type', 'application/hal+json; charset=utf-8')
        .send(itemWithLink);
    }
  } catch (error) {
    throw error;
  }
};

// Adds a new item
exports.addItem = async (request, response) => {
  try {
    const id = uuid.v4();
    const item = new ItemsModel({ ...request.body, _id: id });
    await item.save();
    request.log.info(`ID: ${id} added.`);

    response
      .code(201)
      .header('Content-Type', 'application/hal+json; charset=utf-8')
      .send({ _links: { self: { href: `/api/items/${id}` } } });
  } catch (error) {
    throw error;
  }
};

// Updates an existing item
exports.updateItem = async (request, response) => {
  try {
    const id = request.params.id;
    const item = await ItemsModel.findByIdAndUpdate(id, request.body, {
      lean: true
    });
    if (item === null) {
      response.status(404);
      throw new Error(
        `The item with the id ${id} does not exist in the database.`
      );
    } else {
      request.log.info(`ID: ${id} updated.`);
      response
        .code(200)
        .header('Content-Type', 'application/hal+json; charset=utf-8')
        .send({ _links: { self: { href: request.url } } });
    }
  } catch (error) {
    throw error;
  }
};

// Deletes an item
exports.deleteItem = async (request, response) => {
  try {
    const id = request.params.id;
    const deletedItem = await ItemsModel.findByIdAndRemove(id).lean();

    if (deletedItem === null) {
      request.log.info(`ID: ${id} does not exist in the database.`);
      response.status(404);
      throw new Error(
        `The item with the id ${id} does not exist in the database.`
      );
    } else {
      request.log.info(`ID: ${id} deleted from the database.`);
      response.code(204);
    }
  } catch (error) {
    throw error;
  }
};
