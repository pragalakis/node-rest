const uuid = require('uuid');

function requestParamsIdValidation(request, response, done) {
  if (!uuid.validate(request.params.id)) {
    response.code(400);
    done(new Error('Request parameter id is not a UUID.'));
  }
  done();
}

module.exports = requestParamsIdValidation;
